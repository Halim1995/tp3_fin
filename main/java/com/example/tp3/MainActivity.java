package com.example.tp3;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

public class MainActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    private SwipeRefreshLayout refreshLayout;
    private WeatherDbHelper dbHelper;
    private SimpleCursorAdapter cursorAdapter;
    private Cursor cursor;
    public static String ERROR_MSG_CONNECTION = "Check your network connection";

    public static final String SELECTED_CITY = "city";
    private static final int NEW_CITY_ACTIVITY_REQUEST_CODE = 42;
    private static final int CITY_ACTIVITY_REQUEST_CODE = 43;
    private ListView listView;


    public static boolean isOnline(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        refreshLayout = findViewById(R.id.refresh_layout);
        refreshLayout.setOnRefreshListener(this);
        listView = findViewById(R.id.list_cities);
        dbHelper = new WeatherDbHelper(this);
        dbHelper.populate();
        cursor = dbHelper.fetchAllCities();
        cursorAdapter = new SimpleCursorAdapter(this,
                android.R.layout.simple_list_item_2,
                cursor,
                new String[]{WeatherDbHelper.COLUMN_CITY_NAME, WeatherDbHelper.
                        COLUMN_COUNTRY},
                new int[]{android.R.id.text1, android.R.id.text2}, 0);
        listView.setAdapter(cursorAdapter);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, NewCityActivity.class);
                startActivityForResult(intent, NEW_CITY_ACTIVITY_REQUEST_CODE);


            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                cursor.moveToPosition(position);
                City city = dbHelper.cursorToCity(cursor);
                Intent intent = new Intent(MainActivity.this, CityActivity.class);
                intent.putExtra(SELECTED_CITY, city);
                startActivityForResult(intent, CITY_ACTIVITY_REQUEST_CODE);

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (CITY_ACTIVITY_REQUEST_CODE == requestCode && RESULT_OK == resultCode) {
            City updatedCity = data.getParcelableExtra(MainActivity.SELECTED_CITY);
            dbHelper.updateCity(updatedCity);
            cursor = dbHelper.fetchAllCities();
            cursorAdapter.changeCursor(cursor);
            cursorAdapter.notifyDataSetChanged(); }
        if (NEW_CITY_ACTIVITY_REQUEST_CODE == requestCode && RESULT_OK == resultCode) {
            City newCity = data.getParcelableExtra(NewCityActivity.BUNDLE_EXTRA_NEW_CITY);
            dbHelper.addCity(newCity);
            cursor = dbHelper.fetchAllCities();
            cursorAdapter.changeCursor(cursor);
            cursorAdapter.notifyDataSetChanged();}
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRefresh() {
        if (MainActivity.isOnline(this))
            new UpdateCitiesWheather().execute();
        else {
            Snackbar.make(findViewById(R.id.refresh_layout), ERROR_MSG_CONNECTION, Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            refreshLayout.setRefreshing(false);
        }

    }

    class UpdateCitiesWheather extends AsyncTask<Object, Integer, List<City>> {
        private List<City> mCityList;

        @Override
        protected void onPreExecute() {
            mCityList = dbHelper.getAllCities();
            super.onPreExecute();
        }

        @Override
        protected List<City> doInBackground(Object... objects) {
            for (City city : mCityList) {
                weatherRequest(city);
                dbHelper.updateCity(city);
            }
            return mCityList;
        }


        @Override
        protected void onPostExecute(List<City> cities) {
            cursor = dbHelper.fetchAllCities();
            cursorAdapter.changeCursor(cursor);
            cursorAdapter.notifyDataSetChanged();
            refreshLayout.setRefreshing(false);
            super.onPostExecute(cities);

        }

        private void weatherRequest(City city) {
            HttpURLConnection urlConnection = null;
            try {
                URL urlToRequest = WebServiceUrl.build(city.getName(), city.getCountry());
                urlConnection = (HttpURLConnection) urlToRequest.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.connect();
                InputStream inputStream = urlConnection.getInputStream();
                JSONResponseHandler jsonHandler = new JSONResponseHandler(city);
                jsonHandler.readJsonStream(inputStream);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                urlConnection.disconnect();
            }
        }
    }
}
